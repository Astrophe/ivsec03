<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bug extends Model
{
    // Table Name
    protected $table = 'bugs';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = false;

    public function submission() {
        return $this->belongsTo('App\Submission');
    }
}