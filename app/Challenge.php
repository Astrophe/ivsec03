<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    // Table Name
    protected $table = 'challenges';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    public function submissions() {
        return $this->hasMany('App\Submission');
    }
}
