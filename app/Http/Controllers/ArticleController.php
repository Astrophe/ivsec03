<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{

    public function __construct() {
        $this->middleware('admin', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        return view("articles.index")->with("articles", $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!auth()->user()->isadmin)
            return redirect('/');

        return view("articles.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!auth()->user()->isadmin)
            return redirect('/');

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:191',
            "preview" => "required|max:500",
            "content" => "required|max:3000",
        ]);

        if ($validator->fails()) {
            return redirect('articles/create')
                ->withErrors($validator)
                ->withInput();
        }

        $article = new Article();
        $article->title = $request->input("title");
        $article->preview = $request->input("preview");
        $article->content = $request->input("content");
        $article->image = $request->input("image");

        $article->save();

        return redirect("/")->with("success", "Article added");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {

//        if($article == null)
//            return redirect('/articles');

        return view('articles.show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        if(auth()->user()->isadmin){

            // get the challenge

            // show the edit form and pass the challenge
            return view('articles.edit')->with('article', $article);

        } else {
            return redirect('/article');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        if(auth()->user()->isadmin){

            $validator = Validator::make($request->all(), [
                'title' => 'required|max:191',
                "preview" => "required|max:500",
                "content" => "required|max:3000",
            ]);

            if ($validator->fails()) {
                return redirect()->route('articles.edit', $article->id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $article->title = Input::get('title');
            $article->preview = Input::get('preview');
            $article->content = Input::get('preview');
            $article->image = Input::get('image');
            $article->save();

            // redirect
            Session::flash('message', 'Successfully updated article!');
            return view('articles.show')->with('article', $article);
        } else {
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {

        // Check if user is owner of the bug and if the bug exists
        if($article != null && auth()->user()->isadmin) {
            $article->delete();

            return redirect('/');
        }

        return redirect("/");
    }
}
