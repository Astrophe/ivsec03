<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        login as baseLogin;
    }

    public static function recvCaptchaResponse($token)
    {
        $results = null;
        // send generated token and private key to google for verification

        if ($token) {
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => '6LeTEDwUAAAAAKFjfFCP2jvOLxZiXhdpre06DSt3',
                    'response' => $token
                )
            ]);
            // Decode response message to use within php
            $results = json_decode($response->getBody()->getContents());

            return $results;
        }

        return $results;
    }


    public function login(Request $request)

    {
            // receive token from form and save in variable
            $token = $request->input('g-recaptcha-response');
            // verify token by google and save the results in the variable results
            $results = $this::recvCaptchaResponse($token);

            // if token is successfully verified by google and the login account is activated; log in!
            if($results->success){
                // Captcha succeed

                // Check if account is activated
                $user = User::all()->where('email', $request->input('email'))->first();

                // Redirect if email doesn't exist
                if(!$user)
                    return redirect('/login');
                else if($user->acode != null)
                    // User isn't activated yet, return
                    return redirect('/login');

                return $this->baseLogin($request);
            } else {
                // Captcha is invalid
                return redirect('/login');
            }

        }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
