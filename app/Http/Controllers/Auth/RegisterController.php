<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    // Override method to avoid logging in
    public function register(Request $request)
    {
        // receive token from form and save in variable
        $token = $request->input('g-recaptcha-response');
        // verify token by google and save the results in the variable results
        $results = LoginController::recvCaptchaResponse($token);

        // if token is successfully verified by google; validate the form!
        if($results->success) {
            $this->validator($request->all())->validate();

            event(new Registered($user = $this->create($request->all())));

            return $this->registered($request, $user)
                ?: redirect($this->redirectPath());
        }
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {

        return Validator::make($data, [

            'name' => 'nullable|string|max:255',
            'lastname' => 'nullable|string|max:255',
            'username' => 'required|string|unique:users',
            'phone_number' => 'nullable|string|max:32',
            'date_of_birth' => 'required|before:'.date('y-m-d').'|date',
            'gender' => 'nullable|string|max:1',
            'country' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Generate activation code
        $key = $data['name']."".rand(0, 10000);
        $code = md5($key);

        // Url depends on domain
        $url = "https://ivsec.dev/activate/".$code;

        // Send activation link to user email
        // Mail::to($data['email'])->send(new \App\Mail\ActivationMail($data['username'], $code));
        //dd($data);
        User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'phone_number' => $data['phone_number'],
            'date_of_birth' => $data['date_of_birth'],
            'gender' => $data['gender'],
            'country' => $data['country'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'acode' => $code,
        ]);

        // Show activation url
        dd($url);

        redirect('/login');
    }

    public function Activate($code)
    {
        if($code == null)
            return redirect('/login');

        // Find user with this code and set it to NULL
        $user = User::all()->where('acode', $code)->first();

        if($user != null)
        {
            // Code is valid remove it, now it's activated
            $user->acode = null;
            $user->save();
        }

        return redirect('/login');
    }
}
