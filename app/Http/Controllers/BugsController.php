<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bug;

class BugsController extends Controller
{
    public function __construct()
    {
        // Setup permissions
        $this->middleware('auth');

        $this->middleware('admin', ['only' => ['update']]);

        $this->middleware('owner:submission', ['only' => ['store']]);
        $this->middleware('owner:bug', ['only' => ['destroy']]);
    }

    public function index()
    {

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $submission_id = $request->input('submission_id');

        $bug = new Bug();
        $bug->name = $request->input('name');
        $bug->description = $request->input('description');
        $bug->code = $request->input('editor_value');
        $bug->submission_id = $submission_id;
        $bug->save();

        return redirect('/submissions/'.$submission_id);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $bug = Bug::find($id);

        // If bug not found
        if($bug == null)
            return redirect('/challenges');

        $bug->points = $request->input('points');
        $bug->save();

        return redirect('/review/'.$bug->submission_id);
    }

    public function destroy($id)
    {
        $bug = Bug::find($id);

        $submission_id = $bug->submission_id;
        $bug->delete();

        return redirect('/submissions/'.$submission_id);
    }
}
