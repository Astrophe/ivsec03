<?php

namespace App\Http\Controllers;

use App\Challenge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class ChallengeController extends Controller
{
    public function __construct()
    {
        // Setup permissions
        $this->middleware('auth', ['except' => ['index']]);

        $this->middleware('admin', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $challenges = Challenge::orderBy("created_at", "desc")->paginate(5);

        return view("challenges.index")->with("challenges", $challenges);
    }

    public function create()
    {
        return view("challenges.create");
    }

    public function store(Request $request) {
        $this->validate($request, [
            "title" => "required"
        ]);

        $challenge = new Challenge();
        $challenge->title = $request->input("title");
        $challenge->description = $request->input("description");
        $challenge->startDate = $this->parseFormDate($request->input("start"));
        $challenge->endDate = $this->parseFormDate($request->input("end"));

        $challenge->save();

        return redirect("/challenges")->with("success", "Challenge added");
    }

    public function show($id) {
        // Show challenge using id
        $challenge = Challenge::find($id);

        if($challenge == null)
            return redirect('/challenges');

        return view('challenges.show')->with('challenge', $challenge);
    }

    public function edit($id) {
        // get the challenge
        $challenge = Challenge::find($id);

        // show the edit form and pass the challenge
        return view('challenges.edit')->with('challenge', $challenge);
    }

    public function update($id) {
        $challenge = Challenge::find($id);
        $challenge->title = Input::get('title');
        $challenge->description = Input::get('description');
        $challenge->save();

        // redirect
        Session::flash('message', 'Successfully updated challenge!');
        return view('challenges.show')->with('challenge', $challenge);
    }

    public function destroy($id) {

    }

    private function parseFormDate($date) {
        return str_replace("-", "", $date);
    }
}