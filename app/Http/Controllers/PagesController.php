<?php

namespace App\Http\Controllers;

use App\Challenge;
use Carbon\Carbon;
use App\Article;

class PagesController extends Controller
{
    public function index() {

        $articles = Article::all();

        // order challenge by enddate, make sure enddate is before today to make sure challenge is completed, get last completed challenge.
        $challenge = Challenge::orderBy("endDate", "desc")->whereDate('endDate', '<', Carbon::today())->first();

        if ($challenge === null) {
            // Return view without leaderboard
            return view('pages.index')
                ->with('articles', $articles);
        } else {
            // Get all submission that belong to challenge
            $submissions = $challenge->submissions;

            $scoreUsername = [];
            $scorePoints = [];
            $index = 0;

            // Get all bugs from submission paired with user
            foreach ($submissions as $submission){
                $user = $submission->user;
                $bugs = $submission->bugs;

                // put sum of submission points in score
                $score = 0;
                foreach ($bugs as $bug){
                    $score += $bug->points;
                }

                $scoreUsername[$index] = $user->username;
                $scorePoints[$index] = $score;
                $index++;
            }

            // Sort the arrays synchronically
            array_multisort($scorePoints, SORT_DESC, $scoreUsername);

            // Return view with leaderboard
            return view('pages.index')
                ->with('challenge', $challenge)
                ->with('articles', $articles)
                ->with('scoreUsername', $scoreUsername)
                ->with('scorePoints', $scorePoints);
        }
    }

    public function about() {
        return view('pages.about');
    }
}
