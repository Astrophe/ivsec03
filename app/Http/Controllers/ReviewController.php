<?php

namespace App\Http\Controllers;

use App\Submission;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function __construct()
    {
        // Setup permissions
        $this->middleware('auth');

        $this->middleware('admin');
    }

    public function review($id)
    {
        // Fetch bugs and challenge from submission
        $submission = Submission::find($id);
        $bugs = $submission->bugs()->paginate(2);
        $challenge = $submission->challenge;

        return view('submissions.review')->with(['bugs' => $bugs, 'challenge' => $challenge]);
    }
}
