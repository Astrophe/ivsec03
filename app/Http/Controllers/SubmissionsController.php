<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Submission;
use Illuminate\Support\Facades\DB;

class SubmissionsController extends Controller
{
    public function __construct()
    {
        // Setup permissions
        $this->middleware('auth');

        $this->middleware('admin', ['only' => ['index']]);

        $this->middleware('owner:submission', ['only' => ['show', 'update']]);
    }

    public function index()
    {
        // Show all finished submissions
        $submissions = Submission::orderBy('updated_at', 'desc')->where('submitted', 1)->paginate(10);

        return view('submissions.index')->with('submissions', $submissions);
    }

    public function create()
    {
        // Niet nodig
        return redirect('/challenges');
    }

    public function store(Request $request)
    {
        $challenge_id = $request->input('challenge_id');
        $user_id = auth()->user()->id;

        // Check if this challenge already exists
        if(auth()->user()->getSubmissionFromChallenge($challenge_id) != null)
            return redirect('/challenges');

        // Create submission
        $submission = new Submission();
        $submission->challenge_id = $challenge_id;
        $submission->user_id = $user_id;
        $submission->save();

        return redirect('/submissions/'.$submission->id);
    }

    public function show($id)
    {
        $submission = auth()->user()->submissions->find($id);
        $challenge = $submission->challenge;

        // Check if submission is submitted
        if($submission->submitted)
            return redirect('/challenges');

        $bugs = $submission->bugs;

        return view('submissions.show')->with(['submission' => $submission, 'challenge' => $challenge, 'bugs' => $bugs]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        // Set submission to submitted
        $submission = Submission::find($id);
        $submission->submitted = 1;
        $submission->save();

        // TODO: Send confirmation message to /challenges page

        return redirect('/challenges');
    }

    public function destroy($id)
    {
        //
    }
}
