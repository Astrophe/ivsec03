<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('owner:profile', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index')->with('user',Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = auth()->user();

        // show the edit form and pass the user
        return view('users.edit')->with('user', $user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $validator = Validator::make($request->all(), [
            'date_of_birth' => 'required|before:'.date('y-m-d').'|date',
            'country' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('profiles.edit', $user->id)
                ->withErrors($validator)
                ->withInput();
        }

        if($request->hasFile('avatar')){
            $user->avatar = $this->update_avatar($request);
        }

        $user->name             = Input::get('name');
        $user->lastname         = Input::get('lastname');
        $user->phone_number     = Input::get('phone_number');
        $user->date_of_birth    = Input::get('date_of_birth');
        $user->country          = Input::get('country');
        $user->gender           = Input::get('gender');
        $user->save();

        // redirect
        Session::flash('message', 'Successfully updated user!');
        return Redirect::to('profiles');
    }

    /**
     * Update the avatar in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_avatar(Request $request) {
        // Handle the user upload of avatar
        if($request->hasFile('avatar')){

            // Delete current image before uploading new image
            if (Auth::user()->avatar !== 'default.jpg') {
                $file = public_path('uploads/avatars/' . Auth::user()->avatar);

                if (File::exists($file)) {
                    unlink($file);
                }
            }

            // Get avatar, fit and save it to directory
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->fit(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

            return $filename;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
