<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Let the user pass if he is admin
        if(!auth()->user()->isadmin)
        {
            return redirect('/');
        } else {
            return $next($request);
        }
    }
}
