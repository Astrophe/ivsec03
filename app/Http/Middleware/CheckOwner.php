<?php

namespace App\Http\Middleware;

use App\Bug;
use Closure;

class CheckOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $ownerOf)
    {
        $isOwner = false;

        // Set of rules
        switch($ownerOf)
        {
            case 'bug':
                $bugId = $request->route('bug');
                $isOwner = auth()->user()->hasBug($bugId);
                break;

            case 'submission':
                $submissionId = $request->submission_id;
                if(!$submissionId)
                    $submissionId = $request->route()->parameters['submission'];

                $isOwner = auth()->user()->hasSubmission($submissionId);
                break;

            case 'profile':
                $profileUserId = $request->profile;

                if(!$profileUserId)
                    $profileUserId = $request->route()->parameters['user'];

                $isOwner = $profileUserId == auth()->user()->id;
                break;

            default:
                // No valid parameter is given
                return redirect('/');
                break;
        }

        if($isOwner)
        {
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}
