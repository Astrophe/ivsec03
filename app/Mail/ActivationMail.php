<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $username;
    public $acode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $acode)
    {
        $this->username = $username;
        $this->acode = $acode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@spotthebug.com')
            ->view('mails.activation');
    }
}
