<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    // Table Name
    protected $table = 'submissions';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function challenge() {
        return $this->belongsTo('App\Challenge');
    }

    public function bugs() {
        return $this->hasMany('App\Bug');
    }
}