<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'acode', 'username', 'lastname', 'gender','country','date_of_birth','phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function submissions() {
        return $this->hasMany('App\Submission');
    }

    public function hasSubmission($id) {

        $submission = Submission::find($id);

        return ($submission->user_id == $this->id);
    }

    public function hasBug($bugId)
    {
        $bug = Bug::find($bugId);
        $bugOwnerId = $bug->submission->user_id;

        return($this->id == $bugOwnerId);
    }

    public function getSubmissionFromChallenge($challenge_id) {
        foreach($this->submissions as $submission) {
            if($submission->challenge_id == $challenge_id)
                return $submission;
        }
    }
}
