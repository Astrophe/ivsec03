var editor = document.getElementById('editor');
var copied_value = "";

document.addEventListener('mouseup', function() {
    copied_value = getSelectedText();
    if(document.activeElement.id != editor.id || copied_value == "")
        return;

    document.getElementById('editor_value').value = copied_value;
});

function getSelectedText() {
    if (window.getSelection) {
        txt = window.getSelection();
    } else if (window.document.getSelection) {
        txt = window.document.getSelection();
    } else if (window.document.selection) {
        txt = window.document.selection.createRange().text;
    }
    return txt;
}