@extends('layouts.main')

@section('content')

    <h1>Add article</h1>

    {!! Form::open(['action' => 'ArticleController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

    <div class="form-group">
        {{Form::label('image', 'Image link')}}

        {{Form::text('image', '', ['class' => 'form-control'])}}
    </div>

    <div class="form-group">
        {{Form::label('title', 'Title')}}

        @if ($errors->has('title'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ $errors->first('title') }}</li>
                </ul>
            </div>
        @endif

        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>

    <div class="form-group">
        {{Form::label('preview', 'Preview text')}}

        @if ($errors->has('preview'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ $errors->first('preview') }}</li>
                </ul>
            </div>

        @endif

        {{Form::textarea('preview', '', ['class' => 'form-control', 'placeholder' => '', 'size' => "30x3"])}}
    </div>

    <div class="form-group">
        {{Form::label('content', 'Content')}}

        @if ($errors->has('content'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ $errors->first('content') }}</li>
                </ul>
            </div>

        @endif

        {{Form::textarea('content', '', ['class' => 'form-control', 'placeholder' => '', 'size' => "30x14"])}}
    </div>

    <div>
        <a href="/" class="btn btn-danger">Cancel</a>

        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>

@endsection