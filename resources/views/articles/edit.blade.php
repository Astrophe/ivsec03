@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">

                <h2>Edit article </h2>

                <div class="col-md-10 col-md-offset-0">

                    {{ Form::model($article, array('route' => array('articles.update', $article->id), 'files' => true, 'method' => 'PUT')) }}

                    <div class="form-group">
                    {{ Form::label('image', 'Image link') }}
                    {{ Form::text('image', null, array('class' => 'form-control')) }}
                     </div>

                    <div class="form-group">
                        {{ Form::label('title', 'Title') }}

                        @if ($errors->has('title'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{ $errors->first('title') }}</li>
                                </ul>
                            </div>
                        @endif

                        {{ Form::text('title', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('preview', 'Preview text') }}

                        @if ($errors->has('preview'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{ $errors->first('preview') }}</li>
                                </ul>
                            </div>

                        @endif

                        {{ Form::textarea('preview', null, array('class' => 'form-control', 'placeholder' => '', 'size' => "30x3")) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('content', 'Content') }}

                        @if ($errors->has('content'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{ $errors->first('content') }}</li>
                                </ul>
                            </div>

                        @endif

                        {{ Form::textarea('content', null, array('class' => 'form-control', 'placeholder' => '', 'size' => "30x14")) }}
                    </div>

                    <a href="/articles/{{$article->id}}" class="btn btn-danger">Cancel</a>
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                    {{ Form::close() }}

                </div>
        </div>
    </div>
@endsection