@extends('layouts.main')

@section('content')
    <h1>Articles</h1>
    @if(count($articles) > 0)
        @foreach($articles as $article)
            <div class="well">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="challenges/{{$articles->id}}">{{$articles->title}}</a></h3>

                    @if(!auth()->guest())
                        @if(auth()->user()->getSubmissionFromChallenge($articles->id) == null)

                            <!-- User didn't subscribe this challenge -->
                            {!! Form::open(['action' => ['SubmissionsController@store'], 'method' => 'POST']) !!}
                                {{Form::hidden('articles_id', $articles->id)}}
                            {!! Form::close() !!}
                        @elseif(auth()->user()->getSubmissionFromChallenge($articles->id)->submitted)
                            <!-- User already submitted -->
                            <i>You already submitted this article, your submission will be reviewed soon!</i>
                        @else
                            <a class="btn btn-link" href="/submissions/{{auth()->user()->getSubmissionFromChallenge($articles->id)->id}}">Go to article</a>
                        @endif
                    @endif
                    </div>
                </div>
            </div>
        @endforeach
        {{$articles->links()}}
    @else
        <p>No articles found, OR HAVE THEY</p>
    @endif
@endsection