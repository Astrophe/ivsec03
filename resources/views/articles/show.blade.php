@extends("layouts.main")

@section("content")

    <a href="/" class="btn btn-default">Back</a>

    <table class="table table-striped table-bordered">

        <h1>{{ $article->title }}</h1>
        <br>

        <img src="{{$article->image}}" alt="">

        <br>
        <br>
        <br>

        <p>{{ $article->content }}</p>
        <br>
    </table>

    @if(auth()->user() && auth()->user()->isadmin)
        <a class="btn btn-small btn-info" href="{{ URL::to('articles/' . $article->id . '/edit') }}">Edit this article</a>
        <br>
        <br>
        {!! Form::open(['action' => ['ArticleController@destroy', $article->id], 'method' => 'POST']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Remove', ['class' => 'btn btn-danger'])}}
        {!! Form::close() !!}
        <br>
        <br>
    @endif

@endsection