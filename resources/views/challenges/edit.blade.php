@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">

                <h2>Edit challenge </h2>

                <div class="col-md-10 col-md-offset-0">

                    {{ Form::model($challenge, array('route' => array('challenges.update', $challenge->id), 'files' => true, 'method' => 'PUT')) }}

                    <div class="form-group">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => '', 'size' => "30x14")) }}
                    </div>

                    <a href="/challenges/{{$challenge->id}}" class="btn btn-danger">Cancel</a>
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                    {{ Form::close() }}
                </div>
        </div>
    </div>
@endsection