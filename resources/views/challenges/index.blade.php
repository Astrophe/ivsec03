@extends('layouts.main')

@section('content')
    <h1>Challenges</h1>
    @if(count($challenges) > 0)
        @foreach($challenges as $challenge)
            <div class="well">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="challenges/{{$challenge->id}}">{{$challenge->title}}</a></h3>

                    @if(!auth()->guest())
                        @if(auth()->user()->getSubmissionFromChallenge($challenge->id) == null)

                            <!-- User didn't subscribe this challenge -->
                            {!! Form::open(['action' => ['SubmissionsController@store'], 'method' => 'POST']) !!}
                                {{Form::hidden('challenge_id', $challenge->id)}}
                                {{Form::submit('Subscribe', ['class' => 'btn btn-link'])}}
                            {!! Form::close() !!}
                        @elseif(auth()->user()->getSubmissionFromChallenge($challenge->id)->submitted)
                            <!-- User already submitted -->
                            <i>You already submitted this challenge, your submission will be reviewed soon!</i>
                        @else
                            <a class="btn btn-link" href="/submissions/{{auth()->user()->getSubmissionFromChallenge($challenge->id)->id}}">Go to challenge</a>
                        @endif
                    @endif
                    </div>
                </div>
            </div>
        @endforeach
        {{$challenges->links()}}
    @else
        <p>No challenges found, OR HAVE THEY</p>
    @endif
@endsection