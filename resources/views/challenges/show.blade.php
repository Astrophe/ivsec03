@extends("layouts.main")

@section("content")

    <a href="/challenges" class="btn btn-default">Back</a>
    <h1>Challenge</h1>

    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td>Title</td>
                <td>{{ $challenge->title }}</td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td>Description</td>
                <td>{{ $challenge->description }}</td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td>Starting date</td>
                <td>{{ $challenge->startDate }}</td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td>Ending date</td>
                <td>{{ $challenge->endDate }}</td>
            </tr>
        </tbody>
    </table>

    @if(auth()->user() && auth()->user()->isadmin)
        <a class="btn btn-small btn-info" href="{{ URL::to('challenges/' . $challenge->id . '/edit') }}">Edit this challenge</a>
    @endif

    {{--<div class="col-md-2">--}}
        {{--<div class="row">Description</div>--}}
        {{--<div class="row">Starting date</div>--}}
        {{--<div class="row">Ending date</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6">--}}
        {{--<div class="row">{{$challenge->description}}</div>--}}
        {{--<div class="row">{{$challenge->startDate}}</div>--}}
        {{--<div class="row">{{$challenge->endDate}}</div>--}}
    {{--</div>--}}

    {{--@if(auth()->user() && auth()->user()->isadmin)--}}
        {{--<a href="/challenges/{{$challenge->id}}/edit" class="btn btn-default">Edit</a>--}}
    {{--@endif--}}

@endsection