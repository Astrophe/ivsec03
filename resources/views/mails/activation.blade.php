<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Spot the bug Registration</title>
</head>
<body>
    <p>Hi {{$username}},</p>
<br/>
    <p>Thank you for registering your spot the bug account. Please click the activation link below to activate your account. </p>
<br/>
    <p><a href="https://ivsec.dev/activate/{{$acode}}">https://ivsec.dev/activate/{{$acode}}</a></p>

    <p>Kind regards,</p>
    <p>Team Spot the Bug</p>
</body>
</html>
