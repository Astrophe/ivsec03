@extends('layouts.main')

@section('content')

    <div class="container-fluid">

        <h1>Welcome to spot the bug!</h1>
        <p>Please login in the menu above to participate the challenge</p>
        <br>

        <div class="row">

            <div class="col-md-6">
            @foreach($articles as $article)
                <div class="thumbnail">
                    <img src="{{$article->image}}" alt="" style="height: 200px; width: 600px;">
                    <div class="caption">
                        <h3><b>{{$article->title}}</b></h3>
                        <p>
                            {{$article->preview}}
                        </p>
                        <p>
                            <a href="/articles/{{$article->id}}" class="btn btn-primary">Read more</a>
                        </p>
                    </div>
                </div>
            @endforeach
            </div>

            <div class="col-md-6">
                <h4>Latest completed challenge:</h4>
                @if(empty($challenge))
                    No completed challenges yet!
                @else
                    <h3><a href="challenges/{{$challenge->id}}">{{$challenge->title}}</a></h3>

                    <table class="table table-striped" style="background-color: #64AACA">
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Name</th>
                                <th>Score</th>
                            </tr>
                        </thead>

                        @for($i = 0; $i < sizeof($scoreUsername); $i++)
                            <tbody>
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$scoreUsername[$i]}}</td>
                                    <td>{{$scorePoints[$i]}}</td>
                                </tr>
                            </tbody>
                        @endfor
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection