@extends('layouts.main')

@section('content')
<div class="container">
    <div class="panel panel-default">

        <div class="row">
            <div class="form-group col-xs-6">
            </div>
        </div>
        <hr/>

        @if(count($submissions) > 0)
            @foreach($submissions as $submission)
                <div class="row">
                    <div class="col-md-offset-2">
                        <a href="/review/{{$submission->id}}"><u>Submission</u> {{$submission->challenge->title}} <u>submitted by:</u> {{$submission->user->name}}</a>
                    </div>
                </div>

                <hr/>
            @endforeach
            <div class="row">
                <div class="col-md-offset-2">
                    {{$submissions->links()}}
                </div>
            </div>
        @else
            <i>There are no completed submissions</i>
        @endif

    </div>
</div>
    {!! Html::script('js/editor.js') !!}
@endsection
