@extends('layouts.main')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="row">
            <div class="form-group col-xs-6">
                <h3><u>{{$challenge->title}}</u></h3>
                <textarea id="editor" name="description" class="form-control" rows="20" readonly>{{$challenge->description}}</textarea>
            </div>
            <div class="form-group col-xs-6">
                <!-- Show submitted bugs -->
                @if(count($bugs) > 0)
                    @foreach($bugs as $bug)
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>Bug name:</td><td>{{$bug->name}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <i>
                                            @if($bug->description)
                                                {{$bug->description}}
                                            @else
                                                {{'No description'}}
                                            @endif
                                        </i>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><pre border="none">{{$bug->code}}</pre></td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- Create ASSIGN POINTS button -->
                                        <div class="col-xs-2">
                                            {!! Form::open(['action' => ['BugsController@update', $bug->id], 'method' => 'PATCH']) !!}
                                                {{Form::text('points', $bug->points, ['class' => 'form-control', 'maxlength' => '2', 'placeholder' => 'points'])}}
                                                {{Form::submit('Add', ['class' => 'btn'])}}
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                    @endforeach
                    {{$bugs->links()}}
                @else
                    <i>This submission does not contain any reported bugs</i>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
