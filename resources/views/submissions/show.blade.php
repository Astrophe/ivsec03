@extends('layouts.main')

@section('content')
<div class="container">
    <div class="panel panel-default">

        <div class="row">
            <div class="form-group col-xs-6">
                <h3><u>{{$challenge->title}}</u></h3>
                <textarea id="editor" name="description" class="form-control" rows="20" readonly>{{$challenge->description}}</textarea>
            </div>
            <div class="form-group col-xs-6">
                <h3>Report bug</h3>
                {!! Form::open(['action' => ['BugsController@store'], 'method' => 'POST']) !!}
                    {{Form::hidden('submission_id', $submission->id)}}
                    {{Form::text('name', '', ['placeholder' => 'Bug name', 'class' => 'form-control'])}}
                    {{Form::textarea('description', '', ['id' => 'description', 'class' => 'form-control', 'rows' => 6, 'placeholder' => 'Description'])}}
                    {{Form::textarea('editor_value', '', ['id' =>'editor_value', 'class' => 'form-control', 'readonly'])}}
                    {{Form::submit('Add bug', ['class' => 'btn btn-default'])}}
                {!! Form::close() !!}

                {!! Form::open(['action' => ['SubmissionsController@update', $submission->id], 'method' => 'PATCH']) !!}
                    {{Form::submit('Submit Challenge', ['class' => 'btn btn-primary'])}}
                {!! Form::close() !!}
            </div>
        </div>

        <hr/>
        <!-- Show submitted bugs -->
        @if(count($bugs) > 0)
            @foreach($bugs as $bug)
                <h3>{{$bug->name}}</h3>

                @if($bug->description)
                    <pre>{{$bug->description}}</pre>
                @else
                    <i>No description</i>
                @endif

                <pre>{{$bug->code}}</pre>
                {!! Form::open(['action' => ['BugsController@destroy', $bug->id], 'method' => 'POST']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Remove', ['class' => 'btn btn-danger'])}}
                {!! Form::close() !!}
            @endforeach
        @endif
    </div>
</div>
    {!! Html::script('js/editor.js') !!}
@endsection
