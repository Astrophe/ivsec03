@extends('layouts.main')

@section('content')
    <h1>Add Challenge</h1>
    {!! Form::open(['action' => 'ChallengeController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('title', 'Title')}}
        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => '', 'size' => "30x2"])}}
    </div>
    <div class="form-group">
        {{Form::label("start", "Start")}}
        {{Form::date("start","",["class" => "form-control"], \Carbon\Carbon::now())}}
        {{Form::label("end", "End")}}
        {{Form::date("end","",["class" => "form-control"], \Carbon\Carbon::now())}}
    </div>
    <div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>

@endsection