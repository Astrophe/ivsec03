@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">

                <h2>Edit profile</h2>

                <div class="col-md-10 col-md-offset-0">

                    {{ Form::model($user, array('route' => array('users.update', $user->id), 'files' => true, 'method' => 'PUT')) }}

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                                </div>
                                <div class="row">
                                    {{ Form::file('avatar', null, array('class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('lastname', 'Lastname') }}
                        {{ Form::text('lastname', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, array('class' => 'form-control', 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('username', 'Username') }}
                        {{ Form::text('username', null, array('class' => 'form-control', 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone_number', 'Phone number') }}
                        {{ Form::text('phone_number', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('gender', 'Gender') }}
                    {{Form::select('gender', array('' => '-','m' => 'Male', 'f' => 'Female'))}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('date_of_birth', 'Date of birth') }}
                        @if ($errors->has('date_of_birth'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{ $errors->first('date_of_birth') }}</li>
                                </ul>
                            </div>
                        @endif
                        {{ Form::text('date_of_birth', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('country', 'Country') }}
                        @if ($errors->has('country'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{ $errors->first('country') }}</li>
                                </ul>
                            </div>
                        @endif
                        {{ Form::text('country', null, array('class' => 'form-control')) }}
                    </div>

                    <a href="/profiles" class="btn btn-danger">Cancel</a>
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                    {{ Form::close() }}
                </div>

    </div>
@endsection