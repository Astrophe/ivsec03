@extends('layouts.main')

@section('content')
    <div class="container">

        <div class="row">

            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">

            <h2>{{ $user->username }}'s Profile</h2>

            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>Lastname</td>
                        <td>{{ $user->lastname }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td>{{ $user->username }}</td>
                    </tr>
                    <tr>
                        <td>Phone number</td>
                        <td>{{ $user->phone_number }}</td>
                    </tr>
                    <tr>
                        <td>Date of birth</td>
                        <td>{{ $user->date_of_birth }}</td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>{{ $user->gender }}</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>{{ $user->country }}</td>
                    </tr>
                </tbody>
            </table>

            <!-- edit this user (uses the edit method found at GET /profiles/{id}/edit -->
            @if(auth()->user())
                <a class="btn btn-small btn-info" href="{{ URL::to('profiles/' . $user->id . '/edit') }}">Edit this user</a>
            @endif
        </div>
    </div>
@endsection