@extends("layouts.main")

@section("content")

    <a href="/challenges" class="btn btn-default">Back</a>
    <h1>{{$challenge->title}}</h1>
    <div class="col-md-2">
        <div class="row">Description</div>
        <div class="row">Starting date</div>
        <div class="row">Ending date</div>
    </div>
    <div class="col-md-6">
        <div class="row">{{$challenge->description}}</div>
        <div class="row">{{$challenge->startDate}}</div>
        <div class="row">{{$challenge->endDate}}</div>
    </div>

    @if(auth()->user() && auth()->user()->isadmin)
        <a href="/challenges/{{$challenge->id}}/edit" class="btn btn-default">Edit</a>
    @endif
@endsection