<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/profile', 'UserController@profile');
Route::post('/profile', 'UserController@update_avatar');

Auth::routes();

// Activation route
Route::get('/activate/{code}', 'Auth\RegisterController@activate');

// Review routes
Route::get('/review/{id}', 'ReviewController@review');

Route::get('/edit', 'UsersController@index');


// Resource routes
Route::resource('submissions', 'SubmissionsController');
Route::resource('bugs', 'BugsController');
Route::resource('challenges', 'ChallengeController');

// Profile routes
Route::resource('profile', 'ProfileController');
Route::resource('profiles', 'UserController');
Route::resource('users', 'UserController');
Route::resource('articles', 'ArticleController');